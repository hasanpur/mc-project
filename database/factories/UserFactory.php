<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => Str::random(10),
    ];
});

$factory->define(App\Model\Country::class, function (Faker $faker) {
    return [
        'country_name' => $faker->country,
    ];
});
$factory->define(App\Model\State::class, function (Faker $faker) {
    return [
        'state_name' => $faker->state,
        'country_id' => function(){
        return \App\Model\Country::all()->random();
        },
    ];
});
$factory->define(App\Model\City::class, function (Faker $faker) {
    return [
        'city_name' => $faker->city,
        'state_id' => function(){
            return \App\Model\State::all()->random();
        },
    ];
});

$factory->define(App\Model\Category::class, function (Faker $faker) {
    return [
        'category_title' => $faker->name(),
        'parent_id' => function(){
            return \App\Model\Category::all()->random();
        },
    ];
});

$factory->define(App\Model\Tag::class, function (Faker $faker) {
    return [
        'tag_title' => $faker->name(),
    ];
});

$factory->define(App\Model\Color::class, function (Faker $faker) {
    return [
        'name' => $faker->colorName,
        'color_code' => $faker->hexColor,
    ];
});
$factory->define(App\Model\Size::class, function (Faker $faker) {
    return [
        'number' => $faker->numberBetween(1,5),
        'category_id' => function(){
            return \App\Model\Category::all()->random();
        },

    ];
});




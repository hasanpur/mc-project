<?php
Auth::routes();
Route::get('test',function (){
    return view('test');
});
// Payment Route
Route::any('/Product/payment', 'panel\PaymentController@payment')->name('product.payment');
Route::any('/Product/payment/checker', 'panel\PaymentController@checker')->name('product.checker');
Route::any('/Cart/AddToCart', 'panel\CartController@addToCart')->name('cart.addToCart');
Route::any('/Cart/removeProductFromCart/{id}', 'panel\CartController@removeProductFromCart')->name('cart.removeProductFromCart');
Route::get('/Cart/list', 'panel\CartController@list')->name('cart.list');
Route::get('/Cart/AllPaymentList', 'panel\CartController@AllPaymentList')->name('cart.AllPaymentList');
Route::get('/Cart/update/{id}', 'panel\CartController@update')->name('cart.update');
Route::get('/Store/Product/list', 'panel\ProductController@list')->name('site.products');
Route::get('/Store/Product/single/{id}', 'panel\ProductController@single')->name('site.singleProduct');
Route::get('/aboutUs', 'panel\SettingController@AboutUs')->name('Setting.AboutUs');
Route::get('/SearchBlog/{article}', 'OptionController@SearchBlog')->name('SearchBlog');

//basket


Route::any('/Cart/AddToCart', 'panel\CartController@addToCart')->name('cart.addToCart');
Route::any('/Cart/removeProductFromCart/{id}', 'panel\CartController@removeProductFromCart')->name('cart.removeProductFromCart');
Route::get('/Cart/list', 'panel\CartController@list')->name('cart.list');
Route::get('/Cart/update/{id}', 'panel\CartController@update')->name('cart.update');

//Site Route
Route::group(['namespace' => 'Site'], function () {
    Route::get('/', 'SiteController@index')->name('site');
    Route::get('/CallmePage', 'SiteController@CallmePage')->name('CallmePage');
    Route::get('/InfoPage', 'SiteController@InfoPage')->name('InfoPage');
    Route::get('/Home', 'SiteController@Home');
    Route::get('/IncomePlan', 'SiteController@IncomePlan');
    Route::get('/ShoppingHelp', 'SiteController@ShoppingHelp');
    Route::get('/Privacy', 'SiteController@Privacy');
    Route::get('/Store', 'SiteController@Store')->name('Store')->middleware('auth');
    Route::get('/SingleProduct/{id}', 'SiteController@SingleProduct')->name('SingleProduct');
    Route::get('/CompareProduct', 'SiteController@CompareProduct')->name('CompareProduct');
    Route::get('/StoreWare', 'SiteController@StoreWare')->name('StoreWare');
    Route::get('/ContactUs', 'SiteController@ContactUs')->name('ContactUs');
    Route::get('/AboutUs', 'SiteController@AboutUs')->name('AboutUs');
    Route::get('/Blog', 'SiteController@Blog')->name('Blog');
    Route::get('/SingleBlog/{id}', 'SiteController@SingleBlog');
    Route::get('/ShoppingBasket', 'SiteController@ShoppingBasket')->name('ShoppingBasket');
//    Route::get('/SingleProduct/{id}', 'SiteController@SingleProduct')->name('SingleProduct');

    Route::get('/ResetPassword', 'SiteController@Reset')->name('Form.Reset');
    Route::get('/Login', 'SiteController@Login')->name('Form.Login');
    Route::get('/Register', 'SiteController@Register')->name('Form.Register');

    //Newsletter Route
    Route::group(['prefix' => 'NewsLetter'], function () {
        Route::any('/store', 'NewsLetterController@store')->name('NewsLetter.store');
        Route::any('/join', 'NewsLetterController@join')->name('NewsLetter.join');
        Route::delete('/left', 'NewsLetterController@left')->name('NewsLetter.left');
        Route::get('/list', 'NewsLetterController@list')->name('NewsLetter.list');
        Route::get('/newsLetterUserList', 'NewsLetterController@newsLetterUserList')->name('NewsLetter.newsLetterUserList');
        Route::get('/userNewsLetter', 'NewsLetterController@userNewsLetter')->name('NewsLetter.userNewsLetter');//check status
    });
    //ContactUs Route
    Route::group(['prefix' => 'ContactUs'], function () {
        Route::any('/store', 'ContactUsController@store')->name('ContactUs.store');
        Route::get('/list', 'ContactUsController@list')->name('ContactUs.list');
        Route::get('/single/{id}', 'ContactUsController@single')->name('ContactUs.single');
        Route::any('/update/{id}', 'ContactUsController@update')->name('ContactUs.update');
        Route::delete('/delete/{id}', 'ContactUsController@delete')->name('ContactUs.delete');
    });
});

Route::group(['namespace' => 'Panel', 'prefix' => 'panel', 'middleware' => 'auth'], function () {

    //Image Upload
    Route::group(['prefix' => 'Gallery'], function () {
        Route::any('/showImage/{product_id}', 'ImageController@showImage');
        Route::any('/store/{product_id}', 'ImageController@store');
        Route::delete('/delete/{id}', 'ImageController@delete');
    });

    //Inheritor Route
    Route::group(['prefix' => 'Inheritor'], function () {
        Route::any('/store', 'InheritorController@store')->name('Inheritor.store');
        Route::get('/single/{id}', 'InheritorController@single')->name('Inheritor.single');
        Route::any('/update/{id}', 'InheritorController@update')->name('Inheritor.update');
        Route::delete('/delete/{id}', 'InheritorController@delete')->name('Inheritor.delete');
    });

    //tag Route
    Route::group(['prefix' => 'Tag'], function () {
        Route::any('/store', 'TagController@store')->name('tag.store');
        Route::get('/single/{id}', 'TagController@single')->name('tag.single');
        Route::get('/list', 'TagController@list')->name('tag.list');
        Route::any('/update/{id}', 'TagController@update')->name('tag.update');
        Route::delete('/delete/{id}', 'TagController@destroy')->name('tag.delete');
    });
    //Account Route
    Route::group(['prefix' => 'Account'], function () {
        Route::get('/list', 'AccountController@list')->name('Account.list');
        Route::get('/userActivation', 'AccountController@userActivation')->name('Account.userActivation');
        Route::get('/single', 'AccountController@single')->name('Account.single');
        Route::any('/update/{id}', 'AccountController@update')->name('Account.update');
        Route::delete('/delete/{id}', 'AccountController@destroy')->name('Account.delete');
    });

    //Category Route
    Route::group(['prefix' => 'Category'], function () {
        Route::any('/store', 'CategoryController@store');
        Route::get('/single/{id}', 'CategoryController@single');
        Route::any('/update/{id}', 'CategoryController@update');
        Route::delete('/delete/{id}', 'CategoryController@destroy');
        Route::get('/list', 'CategoryController@list');
    });

    //Help Route
    Route::group(['prefix' => 'Help'], function () {
        Route::any('/store', 'HelpController@store')->name('Help.store');
        Route::get('/list', 'HelpController@list')->name('Help.list');
        Route::get('/single/{id}', 'HelpController@single')->name('Help.single');
        Route::any('/update/{id}', 'HelpController@update')->name('Help.update');
        Route::delete('/delete/{id}', 'HelpController@delete')->name('Help.delete');
    });

    //Blog Route
    Route::group(['prefix' => 'Blog'], function () {
        Route::any('/store', 'BlogController@store')->name('Blog.store');
        Route::get('/list', 'BlogController@list')->name('Blog.list');
        Route::get('/single/{id}', 'BlogController@single')->name('Blog.single');
        Route::any('/update/{id}', 'BlogController@update')->name('Blog.update');
        Route::delete('/delete/{id}', 'BlogController@delete')->name('Blog.delete');
    });

    //Setting Route
    Route::group(['prefix' => 'Setting'], function () {
        Route::get('/list', 'SettingController@AboutUs')->name('Setting.list');
        Route::get('/single/{id}', 'SettingController@single')->name('Setting.single');
        Route::get('/AboutUs', 'SettingController@AboutUs')->name('Setting.AboutUs');
        Route::any('/update/{id}', 'SettingController@update')->name('Setting.update');
    });

    //Favorite Route
    Route::group(['prefix' => 'Favorite'], function () {
        // added nasrin
        Route::any('/favorite/{product}', 'FavoriteController@favorite');
        Route::any('/unfavorites/{product}', 'FavoriteController@unfavorites');
        Route::get('/my_favorites', 'UserController@myFavorites')->middleware('auth');
        // added nasrin

        //added fatemeh
        Route::any('/store', 'FavoriteController@AddFavorite');
        Route::any('/status/{product_id}', 'FavoriteController@FavoriteStatus');
        Route::any('/showLikeIcon/{product_id}', 'FavoriteController@showLikeIcon');
        Route::get('/list', 'FavoriteController@FavoriteList');
        Route::get('/user/list', 'FavoriteController@FavoriteUserList');
        Route::delete('/delete/{product_id}', 'FavoriteController@RemoveFavorite');
        //added fatemeh
    });

    Route::get('/', 'PanelController@index');
    Route::get('/Account', 'PanelController@index');
    Route::get('/AddProduct', 'PanelController@index');
    Route::get('/AllProducts', 'PanelController@index');
    Route::get('/AddArticle', 'PanelController@index');
    Route::get('/Articles', 'PanelController@index');
    Route::get('/Comments', 'PanelController@index');
    Route::get('/Favorites', 'PanelController@index');
    Route::get('/AddTag', 'PanelController@index');
    Route::get('/AddCategory', 'PanelController@index');
    Route::get('/NewsLetterUsers', 'PanelController@index');
    Route::get('/Users', 'PanelController@index');
    Route::get('/OrdersList', 'PanelController@index');
});

//Option Route
Route::group(['prefix' => 'Option'], function () {
    Route::get('/SubCategory', 'OptionController@category');//level1
    Route::get('/Related', 'OptionController@related');
    Route::get('/country', 'OptionController@country');
    Route::any('/state/{country_id}', 'OptionController@state');
    Route::any('/city/{state_id}', 'OptionController@city');
    Route::get('/color', 'OptionController@color');
    Route::get('/tag', 'OptionController@tag');
    Route::get('/sameProduct/{id}', 'OptionController@sameProduct');
    Route::get('/maxScore', 'OptionController@maxScore');
    Route::get('/minScore', 'OptionController@minScore');
    Route::get('/maxPrice', 'OptionController@maxPrice');
    Route::get('/minPrice', 'OptionController@minPrice');

});
Route::group(['prefix' => 'Count'], function () {
    Route::get('/product', 'CounterController@ProductCount');
    Route::get('/comment', 'CounterController@CommentCount');
    Route::get('/category', 'CounterController@CategoryCount');
    Route::get('/user', 'CounterController@UserCount');
    Route::get('/member', 'CounterController@NewsLetterCount');
    Route::get('/user/member', 'CounterController@NewsLetterUserCount');

});

//Product Route
Route::group(['prefix' => 'Product'], function () {
    Route::any('/store', 'panel\ProductController@store');
    Route::get('/single/{id}', 'panel\ProductController@single');//edit
    Route::any('/update/{id}', 'panel\ProductController@update');
    Route::delete('/delete/{id}', 'panel\ProductController@destroy');
    Route::get('/list', 'panel\ProductController@list');
    Route::get('/singleComment/{id}', 'panel\CommentController@singleComment');
    Route::any('/commentUpdate/{id}', 'panel\CommentController@commentUpdate');
    Route::get('/showProductComment/{id}', 'panel\CommentController@showProductComment');
    Route::get('/commentList', 'panel\CommentController@commentList');
    Route::any('/commentStore', 'panel\CommentController@commentStore');
    Route::delete('/commentDelete/{id}', 'panel\CommentController@delete');
});

Route::any('panel/Option/SubCategory/{parent_id}', 'OptionController@subCategory');//level2 , 3 , ...
Route::any('panel/Option/size/{category_id}', 'OptionController@size');


Route::group(['middleware'=>'auth:web'],function (){
    Route::any('/product/increase/{id}', 'Panel\ProductController@increase');
    Route::any('/product/decrease/{id}', 'Panel\ProductController@decrease');

});



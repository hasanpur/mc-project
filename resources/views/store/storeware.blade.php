<html>
<head></head>
    <title>mc - persian-ware property</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-gride.css')}}">
    <link rel="stylesheet" href="{{ asset('css/app.css')}}">
    <link rel="shortcut icon" href="{{ asset('img/logo/logo.png')}}">
<body>
    <div class="continue">
        <div class="ware-property row">
            <div class="col-12 ware-property-main row">
                <div class="ware-property-img col-md-5">
                    <p>فلش مموری مدل قلب</p>
                    <div id="background1" class="ware-property-background">
                        <img src="{{ asset('img/store/ware1.jpg')}}" alt="">
                    </div>
                    <div id="background2" class="ware-property-background" style="display: none">
                        <img src="{{ asset('img/store/products-257-2WatermarkProductPSD.jpg')}}" alt="">
                    </div>
                    <div id="background3" class="ware-property-background" style="display: none">
                        <img src="{{ asset('img/store/products-257-3WatermarkProductPSD.jpg')}}" alt="">
                    </div>
                    <div class="ware-prorerty-gallery">
                        <div id="changeimage1">
                            <img src="{{ asset('img/store/ware1.jpg')}}" alt="">
                        </div>
                        <div id="changeimage2">
                            <img src="{{ asset('img/store/products-257-2WatermarkProductPSD.jpg')}}" alt="">
                        </div>
                        <div id="changeimage3">
                            <img src="{{ asset('img/store/products-257-3WatermarkProductPSD.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
                <div class="ware-property-text col-md-7">
                    <div class="sub-ware-property">
                        <h5>فلش مموری مدل قلب</h5>
                    </div>
                    <div class="ware-property-table">
                        <table class="table table-bordered table-sm">
                            <label for="" class="mb-2">
                                <h6>:مشخصات</h6>
                            </label>
                            <thead>
                            <tr>
                                <th>مارک</th>
                                <th>مدل</th>
                                <th>جنس</th>
                                <th>سایز</th>
                                <th>رنگ</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>مارک</td>
                                <td>مدل</td>
                                <td>جنس</td>
                                <td>سایز</td>
                                <td>رنگ</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="ware-price mb-2">
                        <div class="mb-2">:قیمت</div>
                        <div><p><span>200,000</span> تومان</p></div>
                    </div>
                    <div class="ware-description">
                        <div class="mb-2">:توضیحات</div>
                        <div>
                            <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از
                                صنعت چاپ و با
                                استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون
                            </p>
                        </div>
                    </div>
                    <div class="ware-property-button">
                        <div>
                            <button class="btn btn-outline-success"><i class="fas fa-cart-plus fa-lg" style="margin: 2px"></i>افزودن به سبد خرید</button>
                        </div>
                        <div>
                            <button class="btn btn-outline-success">مقایسه با محصولات دیگر</button>
                        </div>
                    </div>
                    <div class="ware-comparison">
                        <p><h5>:امتیاز به محصول</h5><span> برای امتیاز دهی </span><a href="#">وارد شوید</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row similar-ware">
            <div class="col-12">
                <div class="sub-similar-ware">
                    <h3>محصولات مشابه</h3>
                </div>
                <div class="main-similar-ware row">
                    <div class="col-lg-3 col-md-6">
                        <a href="#">
                            <img src="{{ asset('img/store/ware1.jpg')}}" alt="">
                            <p>لورم ایپسوم متن ساختگی با تولید سادگی</p>
                            <h6>لورم ایپسوم متن ساختگی</h6>
                            <h6><span>90,000</span>تومان</h6>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="#">
                            <img src="{{ asset('img/store/ware2.jpg')}}" alt="">
                            <p>لورم ایپسوم متن ساختگی با تولید سادگی</p>
                            <h6>لورم ایپسوم متن ساختگی</h6>
                            <h6><span>90,000</span>تومان</h6>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="#">
                            <img src="{{ asset('img/store/ware4.jpg')}}" alt="">
                            <p>لورم ایپسوم متن ساختگی با تولید سادگی</p>
                            <h6>لورم ایپسوم متن ساختگی</h6>
                            <h6><span>90,000</span>تومان</h6>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <a href="#">
                            <img src="{{ asset('img/store/ware5.jpg')}}" alt="">
                            <p>لورم ایپسوم متن ساختگی با تولید سادگی</p>
                            <h6>لورم ایپسوم متن ساختگی</h6>
                            <h6><span>90,000</span>تومان</h6>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#changeimage4').click(function () {
                $('.ware-property-background').css("display", "none")
                $('#background4').css("display", "block")
            })
            $('#changeimage3').click(function () {
                $('.ware-property-background').css("display", "none")
                $('#background3').css("display", "block")
            })
            $('#changeimage2').click(function () {
                $('.ware-property-background').css("display", "none")
                $('#background2').css("display", "block")
            })
            $('#changeimage1').click(function () {
                $('.ware-property-background').css("display", "none")
                $('#background1').css("display", "block")
            })
        })
    </script>
</body>

</html>
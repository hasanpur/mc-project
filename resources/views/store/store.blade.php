@extends('approot/app')
@section('title' , ' فروشگاه')
@section('slider')
@endsection
@section('content')
    <store ></store>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#see-more').click(function () {
                if($(this).attr("open-data") == "close"){
                    $('.side-ware').css("height","650px");
                    $(this).attr("open-data" , "open");
                } else {
                    $('.side-ware').css("height","380px");
                    $(this).attr("open-data" , "close");
                }

            });
            $('.sub-sort-right-button').click(function () {
                if($(this).attr("open-on") == "off"){
                    $('.sub-sort-right-button>div').css("right","15px");
                    $(this).attr("open-on" , "on");
                } else {
                    $('.sub-sort-right-button>div').css("right", "5px");
                    $(this).attr("open-on", "off");
                };
            });
        })
    </script>
@endsection

<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<title>
    mc persian - @yield('title')
</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<link rel="stylesheet" href="{{ asset('fontawesome/css/all.css')}}">
<link rel="stylesheet" href="{{ asset('css/bootstrap.css')}}">
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/bootstrap-gride.css')}}">
<link rel="stylesheet" href="{{ asset('css/app.css')}}">
<link rel="stylesheet" href="{{ asset('css/style.css')}}">
<link rel="stylesheet" href="{{ asset('css/responsive.css')}}">
<link rel="shortcut icon" href="{{ asset('img/logo/logo.png')}}">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

<body>
<div id="app">
    <!--start part of header -->
    <div class="container-fluid headerholder sticky-top">
        <div class="container-fluid row body">
            <!-- start of dashboard icon -->
            {{--@if(auth()->check())--}}
            {{--<div class="btn-group">--}}
            {{--<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
            {{--<i class="fa fa-user-circle profileiconheader"></i>--}}
            {{--</button>--}}
            {{--<div class="dropdown-menu">--}}
            {{--<a href="/panel" class="panel-icon">--}}
            {{--پنل کاربری--}}
            {{--</a>--}}
            {{--<form action="{{route("logout")}}" method="POST">--}}
            {{--{{csrf_field()}}--}}

            {{--<button class="btnlogin logout" id="signinbtn">خروج</button>--}}

            {{--</form>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--@else--}}
            {{--<a href="{{route('register')}}">--}}
            {{--<button class="btnlogin">ثبت نام</button>--}}
            {{--</a>--}}
            {{--<a href="{{route('login')}}">--}}
            {{--<button class="btnlogin login">ورود</button>--}}
            {{--</a>--}}
            {{--@endif--}}

            <div class="col-4 dashbordicon">
                @if(auth()->check())
                    <form action="{{route("logout")}}" method="POST">
                        {{csrf_field()}}

                        <button class="btnlogin logout" id="signinbtn">خروج</button>

                    </form>

                    <cart class="shopping-basket-icon"></cart>
                    <a href="/panel" class="panel-icon">
                        <i class="fa fa-user-circle profileiconheader"></i>
                    </a>
                @else
                    <a href="{{route('register')}}">
                        <button class="btnlogin">ثبت نام</button>
                    </a>
                    <a href="{{route('login')}}">
                        <button class="btnlogin login">ورود</button>
                    </a>
                @endif

            </div>
            <!-- end of dashboard icon -->
            <!-- start of header list -->
            <div class="col-6 middle">
                <ul class="headerlist menu-list">
                    <a href="/">
                        <li><p><i class="fa fa-home pl-1"></i>خانه</p></li>
                    </a>
                    <a href="{{auth()->check()?route('Store'):route('login')}}">
                        <li><p><i class="fa fa-store pl-1"></i>فروشگاه</p></li>
                    </a>
                    <a href="{{route('AboutUs')}}">
                        <li><p><i class="fa fa-info pl-1"></i>درباره ی نوین جامه</p></li>
                    </a>
                    <a href="{{route('ContactUs')}}">
                        <li><p><i class="fa fa-envelope pl-1"></i>ارتباط با ما</p></li>
                    </a>
                    <a href="{{route('Blog')}}">
                        <li><p><i class="fa fa-blog pl-1"></i>وبلاگ</p></li>
                    </a>
                </ul>
                <button class="btn responsive-menu-btn open-btn"><i class="fa fa-bars"></i></button>
                <button class="btn responsive-menu-btn close-btn"><i class="fa fa-times"></i></button>
            </div>

            <!-- end of header list -->

            <!-- start of logo -->
            <div class="col-2 mc-logo"><a href="/"><img src="{{ asset('img/logo/logo.png')}}" alt=""
                                                        class="logoimg2"></a></div>
            <div class="col-12 responsive-ul">
                <ul class="headerlist menu-list">
                    <a href="/">
                        <li><p><i class="fa fa-home pl-1"></i>خانه</p></li>
                    </a>
                    <a href="{{auth()->check()?route('Store'):route('login')}}">
                        <li><p><i class="fa fa-store pl-1"></i>فروشگاه</p></li>
                    </a>
                    <a href="{{route('AboutUs')}}">
                        <li><p><i class="fa fa-info pl-1"></i>درباره ی نوین جامه</p></li>
                    </a>
                    <a href="{{route('ContactUs')}}">
                        <li><p><i class="fa fa-envelope pl-1"></i>ارتباط با ما</p></li>
                    </a>
                    <a href="{{route('Blog')}}">
                        <li><p><i class="fa fa-blog pl-1"></i>وبلاگ</p></li>
                    </a>
                </ul>
            </div>
            <!-- end of logo -->
        </div>
    </div>
    <div class="responsive-list">
        <ul class="headerlist">
            <a href="/">
                <li><p><i class="fa fa-home pl-1"></i>خانه</p></li>
            </a>
            <a href="{{auth()->check()?route('Store'):route('login')}}">
                <li><p><i class="fa fa-store pl-1"></i>فروشگاه</p></li>
            </a>
            <a href="{{route('AboutUs')}}">
                <li><p><i class="fa fa-info pl-1"></i>درباره ی نوین جامه</p></li>
            </a>
            <a href="{{route('ContactUs')}}">
                <li><p><i class="fa fa-envelope pl-1"></i>ارتباط با ما</p></li>
            </a>
            <a href="{{route('Blog')}}">
                <li><p><i class="fa fa-blog pl-1"></i>وبلاگ</p></li>
            </a>
        </ul>
    </div>
    <!--end part of header -->
    <!-- start slideshow part -->
@yield('slider')
<!-- ebd slideshow part -->
    <!-- start of content -->
    {{--<div id="app">--}}
    <div class="content">
        @yield('content')
    </div>
    <div class="footer">
        <div class="container-fluid">
            <div class="row">

                @if(auth()->check())
                    {{--To Mrs Marzoghi change style--}}
                    {{--<p> برای عضویت در خبر نامه به <a href=""> حساب کاربری</a> مراجعه کنید. </p>--}}
                    <div class="col-md-4 pt-2 pb-2">
                        <a href="/panel/Account" class="news-letter-register">
                            <button>عضویت در خبرنامه</button>
                        </a>
                    </div>
                @else
                    <form action="{{route('NewsLetter.store')}}" class="searchform col-md-4" method="Post">
                        {{csrf_field()}}
                        <input type="text" name="email"
                               class="searchfield {{ $errors->has('email') ? ' is-invalid' : '' }}"
                               placeholder="درج ایمیل جهت عضویت در خبرنامه">
                        <button class="btn btn-success btnsub"><i class="fa fa-hand-point-up"></i></button>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </form>
                @endif
                <div class="somedia col-md-4 pt-2 pb-2">
                    <a href="">
                        <img src="/img/somedia/instagram.png" alt="" class="soimg">
                    </a>
                    <a href="">
                        <img src="/img/somedia/telegram.png" alt="" class="soimg1">
                    </a>
                </div>
                <div class="telladdress col-md-4 pt-2 pb-2">
                    <span class="addressfield"><i class="fa fa-map-marker-alt"></i> البرز - کرج - میدان نبوت - ساختمان سینا پلاک  117 </span>
                    <span class="tellfield"><i class="fa fa-phone-square"></i> 02633344527 </span>
                </div>
            </div>
        </div>
        <!-- end of footer -->
    </div>
</div>


<!-- end of content -->
<!-- start of footer -->

<script src="{{asset('js/app.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.responsive-menu-btn.open-btn').click(function () {
            $('.responsive-list').slideDown();
            $(this).css('display', 'none')
            $('.responsive-menu-btn.close-btn').css('display', 'block')

        });
        $('.responsive-menu-btn.close-btn').click(function () {
            $('.responsive-list').slideUp();
            $(this).css('display', 'none');
            $('.responsive-menu-btn.open-btn').css('display', 'block')

        })
    });
</script>

</body>

</html>















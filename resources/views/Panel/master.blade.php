<!DOCTYPE html>
<html lang="fa">

<head>
    <base href="/"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>مدیریت وب سایت</title>
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.css')}}">
    <link href="{{asset('/css/app.css')}}" rel="stylesheet">
    {{--<link href="{{asset('/css/style.css')}}" rel="stylesheet">--}}
    <link href="{{asset('/css/panel-style.css')}}" rel="stylesheet">
    <link href="{{asset('/css/responsive.css')}}" rel="stylesheet">

</head>

<body>

<div id="app">
    <nav class="navbar navbar-expand navbar-dark nav-color container-fluid panel-nav">
        {{--<a href="#menu-toggle" id="menu-toggle" class="navbar-brand"><span class="navbar-toggler-icon"></span></a>--}}
        {{--<button class="navbar-toggler" type="button" data-tnavbar-toggler-iconoggle="collapse" data-target="#navbarsExample02"--}}
        {{--aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">--}}
        {{--<span class="navbar-toggler-icon"></span>--}}
        {{--<i class="fa fa-bars"></i>--}}
        {{--</button>--}}
        <button class="btn panel-responsive-btn open-btn">
            <i class="fa fa-bars"></i>
        </button>
        <button class="btn panel-responsive-btn close-btn">
            <i class="fa fa-times"></i>
        </button>
        <span class="panel">پنل مدیریت</span>

        <div class="collapse navbar-collapse" id="navbarsExample02">
            <div class="Hello navbar-nav ">
                <span class="site">
            <a href="/">ورود به سایت</a>
        </span>

                {{--<span>--}}
                {{--سلام--}}
                {{--</span>--}}
                {{--<span class="user-name">--}}
                {{--{{auth()->user()->name}} {{auth()->user()->last_name}}--}}
                {{--</span>--}}
            </div>
        </div>

    </nav>

    <div class="materialContainer modal fade" id="modalLRForm">
        <p class="login-text close-modal-btn" data-dismiss="modal">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-close fa-stack-1x"></i>
            </span>
        </p>
        <form class="login-form">
            <p class="login-text">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-lock fa-stack-1x"></i>
            </span>
            </p>
            <input type="email" class="login-username form-control" autofocus="true" required="true"
                   placeholder="ایمیل یا نام کاربری"/>
            <input type="password" class="login-password form-control" required="true" placeholder="رمز عبور"/>
            <input type="submit" name="Login" value="ورود" class="login-submit form-control submit-btn"/>
            <a href="#" class="login-forgot-pass">رمز عبور خود را فراموش کرده اید؟</a>
        </form>

        <div class="underlay-photo"></div>
        <div class="underlay-black"></div>
    </div>
    <div class="materialContainer modal fade" id="SignUp">
        <p class="login-text close-modal-btn" data-dismiss="modal">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-close fa-stack-1x"></i>
            </span>
        </p>
        <form class="login-form">
            <p class="login-text">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-lock fa-stack-1x"></i>
            </span>
            </p>
            <input style="background: #d3d3d35c" type="email" class="login-username form-control" autofocus="true"
                   required="true" placeholder="ایمیل"/>
            <input style="background: #d3d3d35c" type="text" class="login-username form-control" autofocus="true"
                   required="true" placeholder="نام کاربری"/>
            <input type="password" class="login-password form-control" required="true" placeholder="رمز عبور"/>
            <input type="password" class="login-password form-control" required="true" placeholder="تکرار عبور"/>
            <input type="submit" name="Login" value="ورود" class="login-submit form-control submit-btn"/>
        </form>

        <div class="underlay-photo"></div>
        <div class="underlay-black"></div>
    </div>
    <div id="accordian">
        <ul>
            <li class="text-center pt-3"><img class="panel-user-logo" src="{{auth()->user()->avatar == null ? '/img/user-icon.png':auth()->user()->avatar}}"></li>
            <li class="text-center p-3">
                     <span class="user-name">
          {{auth()->user()->name}} {{auth()->user()->last_name}}
        </span>
            </li>
            <li class="text-center pb-3 marketing-code">
                <span class="">  کد بازاریابی</span>
                <span class="user-name code-marketing">
          {{auth()->user()->username}}
        </span>
            <li class="single" style="border-top: 1px solid #aba5a5;"><h3>
            <li>
                <router-link :to="{name:'FirstPage'}"><i class="fas fa-tachometer-alt pl-1"></i>پیشخوان</router-link>
            </li>
            </h3></li>
            </li>


            <li class="single" ><h3>
            <li >
                <router-link :to="{name:'Account'}"><i class="fa fa-user"></i>حساب کاربری</router-link>
            </li>
            </h3></li>


            <li class="single"><h3>
            <li>
                <router-link :to="{name:'Users'}"><i class="fa fa-users"></i>لیست کاربران</router-link>
            </li>
            </h3></li>


            <li>
                <h3><span class="icon-tasks"></span><i class="fa fa-shopping-bag"></i>محصولات</h3>
                <ul>
                    <li>
                        <router-link :to="{name:'AddProduct'}">افزودن محصول</router-link>
                    </li>
                    <li>
                        <router-link :to="{name:'AllProducts'}">لیست محصولات</router-link>
                    </li>
                </ul>
            </li>


            <li>
                <h3><span class="icon-tasks"></span><i class="fa fa-blog"></i>وبلاگ</h3>
                <ul>
                    <li>
                        <router-link :to="{name:'AddArticle'}">افزودن مطلب</router-link>
                    </li>
                    <li>
                        <router-link :to="{name:'Articles'}">همه مطالب</router-link>
                    </li>
                </ul>
            </li>

            <li>
                <h3><span class="icon-tasks"></span><i class="fa fa-cog"></i>شخصی سازی</h3>
                <ul>
                    <li>
                        <router-link :to="{name:'AddCategory'}">افزودن دسته بندی</router-link>
                    </li>
                    <li>
                        <router-link :to="{name:'AddTag'}">افزودن تگ</router-link>
                    </li>
                </ul>
            </li>
            <li class="single"><h3>
            <li>
                <router-link :to="{name:'OrdersList'}"><i class="fa fa-clipboard-list"></i>لیست سفارش ها</router-link>
            </li>
            </h3></li>
            <li class="single"><h3>
            <li>
                <router-link :to="{name:'Favorites'}"><i class="fa fa-heart"></i>علاقمندی ها</router-link>
            </li>
            </h3></li>
            <li class="single"><h3>
            <li>
                <router-link :to="{name:'Comments'}"><i class="fa fa-comment"></i>نظرات کاربران</router-link>
            </li>
            </h3></li>

            <li class="single"><h3>
            <li>
                <router-link :to="{name:'NewsLetterUsers'}"><i class="fa fa-newspaper"></i>اعضای خبرنامه</router-link>
            </li>
            </h3></li>
        </ul>
    </div>

    <div id="page-wrapper" class="text-center" style="padding-top: 55px">

        <div class="container-fluid">
            {{--<dashboard></dashboard>--}}
            <router-view></router-view>
        </div>
    </div>
</div>
<script src="{{asset('/js/app.js')}}"></script>
<script src="{{asset('/js/vue.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#accordian h3").click(function () {
            //slide up all the link lists
            $("#accordian ul ul").slideUp();
            //slide down the link list below the h3 clicked - only if its closed
            if (!$(this).next().is(":visible")) {
                $(this).next().slideDown();
            }
        })
    });
    $(function () {

        $(".input input").focus(function () {

            $(this).parent(".input").each(function () {
                $("label", this).css({
                    "line-height": "18px",
                    "font-size": "18px",
                    "font-weight": "100",
                    "top": "0px"
                })
                $(".spin", this).css({
                    "width": "100%"
                })
            });
        }).blur(function () {
            $(".spin").css({
                "width": "0px"
            })
            if ($(this).val() == "") {
                $(this).parent(".input").each(function () {
                    $("label", this).css({
                        "line-height": "60px",
                        "font-size": "24px",
                        "font-weight": "300",
                        "top": "10px"
                    })
                });

            }
        });

        $(".button").click(function (e) {
            var pX = e.pageX,
                pY = e.pageY,
                oX = parseInt($(this).offset().left),
                oY = parseInt($(this).offset().top);

            $(this).append('<span class="click-efect x-' + oX + ' y-' + oY + '" style="margin-left:' + (pX - oX) + 'px;margin-top:' + (pY - oY) + 'px;"></span>')
            $('.x-' + oX + '.y-' + oY + '').animate({
                "width": "500px",
                "height": "500px",
                "top": "-250px",
                "left": "-250px",

            }, 600);
            $("button", this).addClass('active');
        });

        $(".alt-2").click(function () {
            if (!$(this).hasClass('material-button')) {
                $(".shape").css({
                    "width": "100%",
                    "height": "100%",
                    "transform": "rotate(0deg)"
                });

                setTimeout(function () {
                    $(".overbox").css({
                        "overflow": "initial"
                    })
                }, 600);

                $(this).animate({
                    "width": "140px",
                    "height": "140px"
                }, 500, function () {
                    $(".box").removeClass("back");

                    $(this).removeClass('active')
                });

                $(".overbox .title").fadeOut(300);
                $(".overbox .input").fadeOut(300);
                $(".overbox .button").fadeOut(300);

                $(".alt-2").addClass('material-buton');
            }

        });

        $(".material-button").click(function () {

            if ($(this).hasClass('material-button')) {
                setTimeout(function () {
                    $(".overbox").css({
                        "overflow": "hidden"
                    });
                    $(".box").addClass("back");
                }, 200);
                $(this).addClass('active').animate({
                    "width": "700px",
                    "height": "700px"
                });

                setTimeout(function () {
                    $(".shape").css({
                        "width": "50%",
                        "height": "50%",
                        "transform": "rotate(45deg)"
                    });

                    $(".overbox .title").fadeIn(300);
                    $(".overbox .input").fadeIn(300);
                    $(".overbox .button").fadeIn(300);
                }, 700);

                $(this).removeClass('material-button');

            }

            if ($(".alt-2").hasClass('material-buton')) {
                $(".alt-2").removeClass('material-buton');
                $(".alt-2").addClass('material-button');
            }

        });



        $('.btn.panel-responsive-btn.open-btn').click(function () {
            $('#accordian').css('marginRight','0');
            $(this).css('display', 'none');
            $('.btn.panel-responsive-btn.close-btn').css('display', 'block');

        });

        $('.btn.panel-responsive-btn.close-btn').click(function () {
            $('#accordian').css('marginRight','-200px');
            $(this).css('display', 'none');
            $('.btn.panel-responsive-btn.open-btn').css('display', 'block')

        });
        if($(window).width() < 1200){
            $('#page-wrapper').click(function () {
                $('#accordian').css('marginRight','-200px');
                $('.btn.panel-responsive-btn.close-btn').css('display', 'none');
                $('.btn.panel-responsive-btn.open-btn').css('display', 'block');

            });
        }

    });
</script>
</body>

</html>

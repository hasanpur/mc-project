import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);
let cart = window.localStorage.getItem('cart');
let cartCount = window.localStorage.getItem('cartCount');
const store = new Vuex.Store({

    state: {
        cart: cart ? JSON.parse(cart) : [],
        cartCount: cartCount ? parseInt(cartCount) : 0,
        Product_id_gallery:'',
    },

    mutations: {
        // addToCart(state, item) {
        addToCart(state,count) {
            // let found = state.cart.find(product => product.id == item.id);
            //
            // if (found) {
            //     found.quantity ++;
            //     found.totalPrice = found.quantity * found.price;
            // } else {
            //     state.cart.push(item);
            //     state.cartCount++;
            //     Vue.set(item, 'quantity', 1);
            //     Vue.set(item, 'totalPrice', item.price);
            // }

            state.cartCount+= count;
            this.commit('saveCart');
            // let data = {
            //     cart: JSON.stringify(this.$store.state.cart)
            // }
            // axios.post('/Cart/AddToCart',data)
        },
        removeFromCart(state, item) {
            let index = state.cart.indexOf(item);

            if (index > -1) {
                let product = state.cart[index];

                state.cartCount -= product.quantity;

                state.cart.splice(index, 1);

            }
            this.commit('saveCart');
        },
        increaseQuantity(state,item){
            let found = state.cart.find(product => product.id == item.id);

            if (found) {
                found.quantity ++;
                state.cartCount ++;
                found.totalPrice = found.quantity * found.price;
            }
            this.commit('saveCart');
        },
        decreaseQuantity(state,item){
            let found = state.cart.find(product => product.id == item.id);

            if (found.quantity > 1) {
                found.quantity --;
                state.cartCount --;
                found.totalPrice = found.quantity * found.price;
            }
            this.commit('saveCart');
        },

        saveCart(state) {
            // window.localStorage.setItem('cart', JSON.stringify(state.cart));
            window.localStorage.setItem('cartCount', state.cartCount);
        }
    },
});
export default store


require('./bootstrap');
window.Vue = require('vue');
window.axios = require('axios');
Vue.use(axios);

let owl_carousel = require('owl.carousel');
window.fn = owl_carousel;

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}


window.$ = require('jquery');
window.JQuery = require('jquery');
import JQuery from 'jquery';
window.$ = JQuery;
window.$ = window.jQuery = require('jquery');



// modules
import router from './Services/Router/Router';
import Vuex from 'vuex';
import SimpleVueValidation from 'simple-vue-validator';
import ZoomOnHover from "vue-zoom-on-hover";
import ProductZoomer from 'vue-product-zoomer';
import VueLazyload from 'vue-lazyload';
import UploadImage from 'vue-upload-image';
import { VueEditor } from "vue2-editor";
import  store from './store'
import VueCarousel from 'vue-carousel';
import Paginate from 'vuejs-paginate'
import CxltToastr from 'cxlt-vue2-toastr'
import 'cxlt-vue2-toastr/dist/css/cxlt-vue2-toastr.css';
import Dashboard from './components/Dashboard';
import Site from './components/Site';


// site
import Banner from './components/Site/Banner.vue';
import Slider from './components/Site/Slider.vue';
import Store from './components/Site/Store.vue';
import SingleProduct from './components/Site/SingleProduct.vue';
import CompareProduct from './components/Site/CompareProduct.vue';
import ShoppingHelp from './components/Site/ShoppingHelp.vue';
import ContactUs from './components/Site/ContactUs.vue';
import AboutUs from './components/Site/AboutUs.vue';
import Blog from './components/Site/Blog.vue';
import SingleBlog from './components/Site/SingleBlog.vue';
import IncomePlan from './components/Site/IncomePlan.vue';
import Privacy from './components/Site/Privacy.vue';

import NewsLetter from './components/Site/NewsLetter.vue';

//panel
import FileInput from './components/Panel/FileInput.vue';
import AddTag from './components/Panel/AddTag.vue';
import AddCategory from './components/Panel/AddCategory.vue';
import Comments from './components/Panel/Comments.vue';
import Favorites from './components/Panel/Favorites.vue';
//vuex
import Cart from './components/Site/Cart.vue';
import ShoppingBasket from './components/Site/ShoppingBasket.vue';

import Login from './components/Login.vue';
import Register from './components/Register.vue';
import ResetPassword from './components/ResetPassword.vue';



// site
Vue.component('Site',Site);
Vue.component('Banner',Banner);
Vue.component('Slider',Slider);
Vue.component('Store',Store);
Vue.component('IncomePlan',IncomePlan);
Vue.component('Privacy',Privacy);
Vue.component('ShoppingHelp',ShoppingHelp);
Vue.component('SingleProduct',SingleProduct);
Vue.component('CompareProduct',CompareProduct);
Vue.component('ContactUs',ContactUs);
Vue.component('AboutUs',AboutUs);
Vue.component('Blog',Blog);
Vue.component('SingleBlog',SingleBlog);
Vue.component('NewsLetter',NewsLetter);
Vue.component('ShoppingBasket',ShoppingBasket);

//vuex
Vue.component('Cart',Cart);
// panel
Vue.component('Dashboard',Dashboard);
Vue.component('Login',Login);
Vue.component('Register',Register);
Vue.component('ResetPassword',ResetPassword);
Vue.component('AddTag',AddTag);
Vue.component('AddCategory',AddCategory);
Vue.component('FileInput',FileInput);
Vue.component('Comments',Comments);
Vue.component('Favorites',Favorites);


// modules
Vue.use(ZoomOnHover);
Vue.use(ProductZoomer);
Vue.use(VueLazyload);
Vue.use(VueEditor);
Vue.component('upload-image', UploadImage);
Vue.use(SimpleVueValidation);
window.Validator = SimpleVueValidation.Validator;
Vue.use(VueCarousel);
Vue.component('paginate', Paginate);
Vue.use(CxltToastr);
var toastrConfigs = {
    position: 'top right',
    showDuration: 2000
};
Vue.use(CxltToastr, toastrConfigs)



window.Vuex = Vuex;
Vue.use(Vuex);

const app = new Vue({
    el: '#app',
    router,
    store
});
export default app;
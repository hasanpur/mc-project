
import Site from '../../components/Site.vue';
import SingleProduct from '../../components/Site/SingleProduct.vue';

const siteUrl = [
    {path:'/Site',name:'Site',component:Site,
        children:[
            {path:'SingleProduct/:id',name:'SingleProduct',component:SingleProduct},
        ]
    },

];
export default siteUrl;
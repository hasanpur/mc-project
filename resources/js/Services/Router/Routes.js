import Dashboard from '../../components/Dashboard.vue';
import Account from '../../components/Panel/Account.vue';
import AddArticle from '../../components/Panel/AddArticle.vue';
import Articles from '../../components/Panel/Articles.vue';
import AddProduct from '../../components/Panel/AddProduct.vue';
import AllProducts from '../../components/Panel/AllProducts.vue';
import AddCategory from '../../components/Panel/AddCategory.vue';
import AddTag from '../../components/Panel/AddTag.vue';
import Comments from '../../components/Panel/Comments.vue';
import Favorites from '../../components/Panel/Favorites.vue';
import SingleProduct from '../../components/Site/SingleProduct.vue';
import SingleBlog from '../../components/Site/SingleBlog.vue';
import NewsLetterUsers from '../../components/Panel/NewsLetterUsers.vue';
import Users from '../../components/Panel/Users.vue';
import FirstPage from '../../components/Panel/FirstPage.vue';
import OrdersList from '../../components/Panel/OrdersList.vue';
const allUrl = [
    {path:'/panel',name:'Dashboard',component:Dashboard,
        children:[
            {path:'/',name:'FirstPage',component:FirstPage},
            {path:'Account',name:'Account',component:Account},
            {path:'AddArticle',name:'AddArticle',component:AddArticle},
            {path:'Articles',name:'Articles',component:Articles},
            {path:'AddProduct',name:'AddProduct',component:AddProduct},
            {path:'AllProducts',name:'AllProducts',component:AllProducts},
            {path:'AddCategory',name:'AddCategory',component:AddCategory},
            {path:'AddTag',name:'AddTag',component:AddTag},
            {path:'Favorites',name:'Favorites',component:Favorites},
            {path:'Comments',name:'Comments',component:Comments},
            {path:'NewsLetterUsers',name:'NewsLetterUsers',component:NewsLetterUsers},
            {path:'Users',name:'Users',component:Users},
            {path:'OrdersList',name:'OrdersList',component:OrdersList},

        ]
    },
    {path:'/SingleProduct/:id',name:'SingleProduct',component:SingleProduct},
    {path:'/SingleBlog/:id',name:'SingleBlog',component:SingleBlog},
];
export default allUrl;
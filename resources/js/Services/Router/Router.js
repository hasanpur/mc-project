import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);
import routes  from './Routes';
// import SiteRoutes  from './SiteRoutes';
const router = new VueRouter({
   mode:'history',
    routes,
    // SiteRoutes
});

export default router
<?php

namespace App;

use App\Model\Product;
use Illuminate\Database\Eloquent\Model;
//use Storage;
use Illuminate\Support\Facades\Storage;

class ImageGallery extends Model
{
    protected $primaryKey = 'image_id';
    protected $fillable = ['file_name'];

    public function getSrcAttribute()
    {
        return Storage::url($this->image_id);
    }
    public function product()
    {
        return $this->belongsToMany(Product::class,'image_gallery_product','image_id','product_id');
    }
}

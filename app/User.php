<?php

namespace App;

use App\Model\Account;
use App\Model\Cart;
use App\Model\City;
use App\Model\Comment;
use App\Model\ContactUs;
use App\Model\Country;
use App\Model\Favorite;
use App\Model\Massage;
use App\Model\NewsLetter;
use App\Model\Payment;
use App\Model\Permission;
use App\Model\Product;
use App\Model\Role;
use App\Model\State;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $fillable = [

        'name',
        'last_name',
        'username',
        'email',
        'mobile',
        'password',
        'birthday_date',
        'national_code',
        'news_letter_status',
        'phone',
        'avatar',
        'country_id',
        'state_id',
        'city_id',
        'status',
        'news_letter_status'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
    public function favorites()
    {
        return $this->belongsToMany(Product::class, 'favorites', 'user_id', 'product_id')->withTimeStamps();
    }
    public function payment()
    {
        return $this->hasMany(Payment::class);
    }

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function cart()
    {
        return $this->hasMany(Cart::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('title', $role);
        }

        return !!$role->intersect($this->roles)->count();
    }

    public function account()
    {
        return $this->hasOne(Account::class);
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'country_id', 'id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'state_id', 'id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'city_id', 'id');
    }

    public function contact()
    {
        return $this->belongsTo(ContactUs::class);
    }

    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

    public function newsLetter()
    {
        return $this->hasOne(NewsLetter::class);
    }

}

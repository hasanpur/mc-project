<?php

namespace App\Model;

use App\User;
use App\Model\Product;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable=[
      'user_id','product_id','count','price','size','color','total_price'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function size()
    {
        return $this->hasOne(Size::class, 'size_id', 'size');
    }

    public function color()
    {
        return $this->hasOne(Color::class, 'color_id', 'color');
    }
    public function product()
    {
        return $this->belongsTo(Product::class);
    }


}

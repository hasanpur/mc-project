<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageGalleyProduct extends Model
{
    protected $table='image_gallery_product';
    protected $fillable=['image_id','product_id'];
}

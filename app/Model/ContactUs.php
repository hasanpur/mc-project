<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $table='contact_uses';
    protected $fillable = ['title', 'description', 'related_id','response','status'];

    public function related_to()
    {
        return $this->hasOne(RelatedTo::class, 'related_id', 'related_id');
    }
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'related_id');
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $primaryKey = 'size_id';
    protected $fillable = ['number', 'category_id'];

//    public $timestamps=false;

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'size_id');
    }
    public function cart()
    {
        return $this->belongsTo(Cart::class,'id','size');
    }
    public function product()
    {
        return $this->belongsToMany(Product::class, 'product_size', 'size_id', 'product_id');

    }


}

<?php

namespace App;

use App\Model\Product;
use Illuminate\Database\Eloquent\Model;
//use Storage;
use Illuminate\Support\Facades\Storage;

class ImageGallery extends Model
{
    protected $primaryKey = 'image_id';
    protected $fillable = ['file_name'];
//    protected $fillable = ['product_id', 'file_name'];

    public function getSrcAttribute()
    {
        return Storage::url($this->image_id);
    }
    public function product()
    {
        return $this->belongsToMany(Product::class,'image_gallery_product','image_id','product_id');
//        return $this->belongsToMany(Product::class,'image_gallery_product','product_id','image_id');
//        return $this->belongsTo(Product::class,'product_id','image_id');
    }
}

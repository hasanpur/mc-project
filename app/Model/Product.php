<?php

namespace App\Model;

use App\ImageGallery;
use Gloudemans\Shoppingcart\Cart;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Product extends Model
{
    protected $fillable = ['name', 'features', 'description', 'price', 'category_id', 'count', 'score','favorite'];

    protected $casts = ['size', 'color', 'score', 'features' => 'array'];

    protected $with=['gallery','category'];

    public function tag()
    {
        return $this->belongsToMany(Tag::class, 'product_tag', 'product_id', 'tag_id');
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class,'product_id','id');
    }
    public function cart()
    {
        return $this->hasMany(Cart::class);
//        return $this->belongsToMany(Cart::class, '', 'cart_id', 'id')->pivot('count', 'unit_price');
    }

    public function gallery()
    {
        return $this->belongsToMany(ImageGallery::class, 'image_gallery_product', 'product_id', 'image_id');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'category_id', 'id');
    }

    public function size()
    {
        return $this->belongsToMany(Size::class, 'product_size', 'product_id', 'size_id');
    }

    public function color()
    {
        return $this->belongsToMany(Color::class, 'color_product', 'product_id', 'color_id');
    }

    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

//    public function favorite()
//    {
//        return $this->hasMany(Favorite::class);
//    }

    public function favorited()
    {
        return (bool) Favorite::where('user_id', Auth::id())
            ->where('product_id', $this->id)
            ->first();
    }
}

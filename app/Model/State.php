<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{

    protected $fillable=['state_name','country_id'];
    protected $primaryKey=['state_id'];
    protected $casts=[
        'state_id'=>'array'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class,'country_id', 'state_id');
    }

    public function city()
    {
        return $this->hasMany(City::class, 'city_id', 'state_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'state_id');
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RelatedTo extends Model
{
    protected $primaryKey = 'related_id';
    protected $fillable = ['related_title'];

    public function contact()
    {
        return $this->belongsTo(ContactUs::class);
    }
}

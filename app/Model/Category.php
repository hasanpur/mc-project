<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $primaryKey="category_id";
    protected $fillable=[
        "category_title",
        "parent_id"
    ];


    public function product()
    {
        return $this->belongsTo(Product::class,'id','category_id');
    }
    public function size()
    {
        return $this->hasMany(Size::class, 'size_id', 'category_id');
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $primaryKey='color_id';
    protected $fillable=['name','color_code'];
    public $timestamps=false;

    public function product()
    {
        return $this->belongsToMany(Product::class,'color_product','color_id','product_id');
    }
    public function cart()
    {
        return $this->belongsTo(Cart::class,'color','id');
    }

}

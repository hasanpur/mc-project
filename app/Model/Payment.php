<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable=['user_id','resnumber','product_id','price','payment'];
    protected $casts=[
        'product_id'=>'array'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function product()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }

}

<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable=[
        'user_id',
        'age',
        'national_code',
        'phone',
        'avatar',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ImageProduct extends Model
{
    protected $fillable=['product_id','imageUrl'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable=['tag_title'];
    protected $primaryKey='tag_id';
    public function product()
    {
        return $this->belongsToMany(Product::class,'product_tag','tag_id','product_id');
//        return $this->belongsToMany(Product::class,'product_tag','product_id','tag_id');
    }
}

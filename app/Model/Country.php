<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable=['country_name'];
    protected $guarded=['country_id'];

    public function state()
    {
        return $this->hasMany(State::class,'state_id','country_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'id','country_id');
    }
}

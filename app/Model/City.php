<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $primaryKey = ['city_id'];
    protected $fillable = ['city_name', 'state_id'];
    protected $casts = [
        'city_id' => 'array'

    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'city_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id', 'city_id');
    }
}

<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class NewsLetter extends Model
{
    protected $fillable=["user_id","email"];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

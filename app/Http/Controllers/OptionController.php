<?php

namespace App\Http\Controllers;

use App\Model\Blog;
use App\Model\Category;
use App\Model\City;
use App\Model\Color;
use App\Model\Country;
use App\Model\Product;
use App\Model\RelatedTo;
use App\Model\Size;
use App\Model\State;
use App\Model\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OptionController extends Controller
{
    public function country()
    {
        $country=Country::all();
        return response($country,200);
    }
    public function state($country_id)
    {
//        $state = State::where('country_id', '=', $country_id)->get();
        $state = DB::table('states')->where('country_id',$country_id)->get();
        return response()->json($state,200);
    }
    public function city($state_id)
    {
//        $city = City::where('state_id', '=', $state_id)->get();
        $city=DB::table('cities')->where('state_id',$state_id)->get();
        return response()->json($city,200);
    }
    public function category()
    {
        return response()->json(Category::all(),200);
    }

    public function subCategory($parent_id)
    {
        $subCategory = Category::where('parent_id', '=', $parent_id)->get();
        return response()->json($subCategory,200);
    }

    public function related()
    {
        return response()->json(RelatedTo::all(), 200);
    }



    public function size($category_id)
    {
        $size = Size::where('category_id', '=', $category_id)->get();
        return response()->json($size,200);
    }

    public function color()
    {
        return response(Color::all(),200);
    }
    public function tag()
    {
        return response(Tag::all(),200);
    }

    public function SearchBlog($article)
    {
        $query = Blog::where('title', 'like', '%' . $article . '%')->get();
        return response()->json($query);
    }

    public function sameProduct($id)
    {
        $product = Product::find($id);
        $same = Product::with('gallery')->where('name', 'like', '%' . $product->name . '%')
            ->orWhere('category_id', '=', $product->category_id)
            ->get();

        return response()->json($same, 200);
    }

    public function maxScore()
    {
        $product=Product::with('color','size','tag')->orderBy('score', 'desc')->get();
        return response()->json($product);
    }
    public function minScore()
    {
        $product=Product::orderBy('score', 'asc')->get();
        return response()->json($product);
    }

    public function maxPrice()
    {
        $product=Product::with('color','size','tag')->orderBy('price','desc')->get();
        return response()->json($product);
    }

    public function minPrice()
    {
        $product=Product::with('color','size','tag')->orderBy('price','asc')->get();
        return response()->json($product);
    }
}



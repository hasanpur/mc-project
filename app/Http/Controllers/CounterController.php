<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Comment;
use App\Model\NewsLetter;
use App\Model\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CounterController extends Controller
{
    public function ProductCount()
    {
        $productCount = count(Product::all());
        return response()->json($productCount,200);
    }

    public function CommentCount()
    {
        $commentCount=count(Comment::all());
        return response()->json($commentCount,200);
    }

    public function UserCount()
    {
        $userCount=count(User::all());
        return response()->json($userCount,200);
    }

    public function CategoryCount()
    {
        $categoryCount=count(Category::all());
        return response()->json($categoryCount,200);

    }
    public function NewsLetterCount()
    {
        $memberCount = count(NewsLetter::all());
        return response()->json($memberCount,200);
    }

    public function  NewsLetterUserCount()
    {
        $memberCount = count(NewsLetter::where('user_id','!=',null)->get());
        return response()->json($memberCount,200);
    }
}

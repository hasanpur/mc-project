<?php

namespace App\Http\Controllers\Panel;

use App\Model\Cart;
use App\Model\Payment;
use App\Model\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{

    public function addToCart(Request $request)
    {
        $id = auth()->id();
        $user = User::find($id);
        $user->cart()->create([
            'product_id' => $request->input('id'),
            'total_price' => $request->input('totalPrice'),
            'count' => 1,
            'price' => $request->input('price'),
            'size' => $request->input('size'),
            'color' => $request->input('color')
        ]);

    }

    public function list()
    {
        $userId = auth()->user()->id;
        $cart = Cart::with('user', 'product', 'color', 'size')->where('user_id', $userId)->get();
        return response()->json(['cart' => $cart], 200);
    }

    public function AllPaymentList()
    {
        $product=Product::all();
        $cart=  $cart = Cart::with('user', 'product', 'color', 'size')->get();
        return response()->json(['payment' => $pay,'product' => $product,'cart' => $cart], 200);

    }

    public function removeProductFromCart($id)
    {
        $cartProduct = Cart::find($id);
        $cartProduct->delete();
        return response()->json(null, 405);
    }

    public function update(Request $request, $id)
    {
        $cartProduct = Cart::find($id);
        $cartProduct->update($request->all());
        return response()->json(['message' => 'success'], 200);

    }

}

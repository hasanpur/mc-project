<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function myFavorites()
    {
        $myFavorites = Auth::user()->favorites;
        return response()->json($myFavorites, 200);
    }
}

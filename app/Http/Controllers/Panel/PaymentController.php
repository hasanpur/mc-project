<?php

namespace App\Http\Controllers\Panel;

use App\Model\Payment;
use App\Model\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use SoapClient;

class PaymentController extends Controller
{
    public $MerchantID = '46688bb0-9217-11e7-bbe1-005056a205be'; //Required

//    public $MerchantID = 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX'; //Required

    public function payment(Request $request)
    {
//        return $request->all();
        $this->validate($request, [
            'product_id' => 'required'
        ]);

        if ($request->input('price') == 0) {
            alert()->error('این محصول قابل خریداری برای شما نیست .', 'error')->persistent('خیلی خوب');
        }

        $MerchantID = $this->MerchantID;
        $Amount = $request->input('price');
        $Description = 'توضیحات تراکنش تستی';
        $Mobile = auth()->user()->mobile;
        $CallbackURL = 'http://localhost:8000/Product/payment/checker';


        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);


        $result = $client->PaymentRequest(
            [
                'MerchantID' => $MerchantID,
                'Amount' => $Amount,
                'Description' => $Description,
                'Mobile' => $Mobile,
                'CallbackURL' => $CallbackURL,
            ]
        );
        $productId = json_decode(\request('product_id'), true);
        if ($result->Status == 100) {
            auth()->user()->payment()->create(array_merge([
                'resnumber' => $result->Authority,
                'price' => $request->input('price'),
                'product_id' => $productId,
            ]));
            return response()->json([
                'link'=>'https://www.zarinpal.com/pg/StartPay/'. $result->Authority,
            ]);
        } else {
            echo 'ERR: ' . $result->Status;
        }
    }

    public function checker()
    {
        $Authority = request('Authority');

        $payment = App\Model\Payment::whereResnumber($Authority)->findOrFail();

        if ($_GET['Status'] == 'OK') {

            $client = new SoapClient('https://zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

            $result = $client->PaymentVerification([
                'MerchantID' => $this->MerchantID,
                'Authority' => $Authority,
                'Amount' => $payment->price,
            ]);

            if ($result->Status == 100) {
                echo 'تراکنش موفقیت آمیز بود . شمارهه پیگیری:' . $result->RefID;
            } else {
                echo 'ترانکش با خطا مواجه شد . وضعیت : ' . $result->Status;
            }
        } else {
            echo 'عملیات پرداخت توسط کاربر لغو شد.';
        }

    }
}

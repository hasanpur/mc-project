<?php

namespace App\Http\Controllers\Panel;

use App\Model\Favorite;
use App\Model\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    public function AddFavorite(Request $request)
    {
        $favorite = $request->all();
        $favorite['user_id'] = auth()->user()->id;
        $favorite['product_id']=$request->input('product_id');
        Favorite::create($favorite);
        return response()->json(['massage' => 'success']);
    }
    public function FavoriteList()
    {
        $favorite=Favorite::with(['user','product'=>function($image){
            $image->with('category','gallery');
        }])->get();
        return response()->json($favorite,200);
    }
    public function FavoriteUserList()
    {
        $user_id = Auth::id();
        $user = User::find($user_id);
        $favorite = Favorite::with('product')->where('user_id','=',$user->id)->get();
        return response()->json($favorite,200);
    }
    public function FavoriteStatus($product_id)
    {
        $id = Auth::id();
        $product=Product::find($product_id);
        $favorite =Favorite::where('user_id', '=', $id)
            ->where('product_id',$product->id)->first();
        if (is_null($favorite)) {
            $answer = '0';
        } else {
            $answer = '1';
        }
        $data = ['favorite_status' => $answer];
        return response()->json($data);
    }
    public function RemoveFavorite($product_id)
    {
        $user_id = Auth::id();
        $user = User::find($user_id);
        $favorite = Favorite::where('user_id','=',$user->id)
        ->where('product_id','=',$product_id)->first();
        $favorite->delete();
        return response()->json(['massage' => 'success'],200);
    }


    public function showLikeIcon($product_id)
    {
        $productId=$product_id;
        $userId=auth()->id();
        $favorite = Favorite::where('user_id', $userId)
            ->where('product_id', $productId)->first();
        if (!empty($favorite)) {
            $answer = 'yes';
        }else{
            $answer = 'no';
        }
        return response()->json($answer);
    }
    public function favorite(Product $product)
    {
        Auth::user()->favorites()->attach($product->id);
        return response()->json(['message'=>'success'],200);

    }
    public function unfavorites(Product $product)
    {
        Auth::user()->favorites()->detach($product->id);
        return response()->json(['message'=>'success'],200);
    }
}

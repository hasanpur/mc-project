<?php

namespace App\Http\Controllers\Panel;

use App\Model\Account;
use App\Model\City;
use App\Model\Country;
use App\Model\State;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AccountController extends Controller
{

    public function list()
    {
        $user=DB::table('users')
            ->join('countries','users.country_id','=','countries.country_id')
            ->join('states','users.state_id','=','states.state_id')
            ->join('cities','users.city_id','=','cities.city_id')
            ->get();
        return response()->json($user);

    }
    public function single()
    {
        $id = auth()->id();
        $user = User::find($id);
        $user['country_id'] = Country::where('country_id', $user->country_id)->first();
        $user['state_id'] = $user->join('states', 'users.id', '=', 'states.state_id')->select('states.*')->first();
        $user['city_id'] = $user->join('cities', 'users.id', '=', 'cities.city_id')->select('cities.*')->first();
        return response()->json($user);
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
        'avatar'=>'nullable|mimes:jpg,jpeg,png,gif|max:1024'
        ]);

        $userId = auth()->id();
        $user = User::find($userId);
        // Upload File
        if ($request->hasFile('avatar')) {
            if (file_exists($user->avatar)) {
                unlink($user->avatar);
            }
            $filename = md5(rand(100000, 999999));
            $file_obj = $request->file('avatar');
            $file_obj->move("uploads\\AccountImg", $filename . "." . $file_obj->getClientOriginalExtension());
            $request->request->add(["avatar" => 'uploads\\AccountImg\\' . $filename . "." . $file_obj->getClientOriginalExtension()]);
            $user->update(['avatar' => $request->input('avatar')]);
        }

        $user->update([
            'name' => $request->input('name'),
            'last_name' => $request->input('last_name'),
            'mobile' => $request->input('mobile'),
            'birthday_date' => $request->input('birthday_date'),
            'national_code' => $request->input('national_code'),
            'phone' => $request->input('phone'),
            'country_id' => $request->input('country_id'),
            'state_id' => $request->input('state_id'),
            'city_id' => $request->input('city_id'),
        ]);


    }

    public function userActivation(Request $request)
    {
        $user = User::find($request->id);
        $user->status = $request->status;
        $user->savئe();
    }
    public function destroy($id)
    {
        $account = Account::find($id);
        if (file_exists($account->avatar)) {
            unlink($account->avatar);
        }
        $account->delete();
    }
}

<?php

namespace App\Http\Controllers\Panel;

use App\Model\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request,[
           'title' =>'required|min:2|max:191',
           'imgUrl' =>'mimes:jpg,png,jpeg,gif|max:3072'
        ]);
        // Upload image
        if ($request->hasFile('imgUrl')) {
            $filename = md5(rand(100000, 999999));
            $file_obj = $request->file('imgUrl');
            $file_obj->move(public_path("uploads\\BlogImage"), $filename . "." . $file_obj->getClientOriginalExtension());
            $request->request->add(["imgUrl" => 'uploads\\BlogImage\\' . $filename . "." . $file_obj->getClientOriginalExtension()]);
        }

        Blog::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'reade_more' => $request->input('reade_more'),
            'imgUrl' => $request->input('imgUrl'),
        ]);
        return response()->json(['message' => 'Success'], 200);
    }

    public function list()
    {
        return response()->json(Blog::all(), 200);
    }

    public function single($id)
    {
        $blog = Blog::find($id);
        return response()->json($blog, 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' =>'required|min:2|max:191',
            'imgUrl' =>'mimes:jpg,png,jpeg,gif|max:3072'
        ]);
        $blog = Blog::find($id);
        // Upload image
        if ($request->hasFile('imgUrl')) {
            if (file_exists($blog->imgUrl)) {
                unlink($blog->imgUrl);
            }
            $filename = md5(rand(100000, 999999));
            $file_obj = $request->file('imgUrl');
            $file_obj->move(public_path("uploads\\BlogImage"), $filename . "." . $file_obj->getClientOriginalExtension());
            $request->request->add(["imgUrl" => 'uploads\\BlogImage\\' . $filename . "." . $file_obj->getClientOriginalExtension()]);
            $blog->update(['imgUrl' => $request->input('imgUrl')]);
        }

        $blog->update([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'reade_more' => $request->input('reade_more'),
        ]);
        return response()->json(['message' => 'Success'], 200);
    }

    public function delete($id)
    {
        $blog = Blog::find($id);
        if (file_exists($blog->imgUrl)) {
            unlink($blog->imgUrl);
        }
        $blog->delete();
        return response()->json(['message' => 'Success'], 200);
    }
}

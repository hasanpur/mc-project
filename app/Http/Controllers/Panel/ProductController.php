<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\ImageGallery;
use App\ImageGalleyProduct;
use App\Model\Cart;
use App\Model\Category;
use App\Model\Favorite;
use App\Model\ImageProduct;
use App\Model\Massage;
use App\Model\Product;
use App\Model\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2|max:191',
            'count' => 'numeric',
            'score' => 'numeric',
            'price' => 'required|numeric',
            'color_id' => 'required',
            'tag_id' => 'required',
            'size_id' => 'required',
        ]);
        $features = json_decode(\request('features'), true);
        $product = Product::create(array_merge([
            'name' => $request->input('name'),
            'features' => $features,
            'description' => $request->input('description'),
            'category_id' => $request->input('category_id'),
            'count' => $request->input('count'),
            'score' => $request->input('score'),
            'price' => $request->input('price'),
        ]));

        $tag = json_decode(\request('tag_id'), true);
        $size = json_decode(\request('size_id'), true);
        $color = json_decode(\request('color_id'), true);
        $product->tag()->sync($tag);
        $product->size()->sync($size);
        $product->color()->sync($color);

        return response()->json(['product_id' => $product->id]);
    }

    public function single($id)
    {
        $user_id = Auth::id();

        $product = Product::find($id);
//        $favorite = $product->favorite()->first();
//        $category = $product->category()->first();
        $favorite = Favorite::where('product_id', $id)
            ->where('user_id', $user_id)->first();
        if (is_null($favorite)) {
            $answer = '0';
        } else {
            $answer = '1';
        }
        $product['gallery'] = $product->gallery()->get();
        $product['tag'] = $product->tag()->get();
        $product['color'] = $product->color()->get();
        $product['size'] = $product->size()->get();
        $product['likeStatus'] = $answer;
        $category = Category::join('products', 'categories.category_id', '=', 'products.category_id')
            ->select('categories.*')->first();
        $product['subCategory'] = $category->category_title;
        $product['category'] = Category::where('category_id', $category->parent_id)->first()->category_title;
        return response()->json($product);

        /*  If top code doesn't work use it
        $product['subCategory'] = $product->category()->first()->category_title;
         $product['category'] = Category::where('category_id', $category->parent_id)->first()->category_title;
         $product['subCategory'] = $product->category()->first()->category_title;*/

    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:2|max:191',
            'count' => 'numeric',
            'score' => 'numeric',
            'price' => 'required|numeric',
            'color_id' => 'required',
            'tag_id' => 'required',
            'size_id' => 'required',
        ]);
        $product = Product::find($id);
        $size = json_decode(\request('size_id'), true);
        $color = json_decode(\request('color_id'), true);
        $tag = json_decode(\request('tag_id'), true);
        $product->tag()->atach($tag);
        $product->size()->sync($size);
        $product->color()->sync($color);
        return response()->json(['massage' => 'success']);
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->gallery()->delete();
        $product->delete();
    }

    public function list()
    {
        $product = Product::with('tag', 'color', 'size')->get();
        return response()->json($product);
    }

    public function increase(Request $request, $id)
    {
        return $this->changeShoppincartPrice($request, $id);
    }

    public function decrease(Request $request, $id)
    {
        return $this->changeShoppincartPrice($request, $id);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeShoppincartPrice(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $cart = Cart::find($id);
        $count = $request->input('count');
        $price = $cart->price;
        $totalPrice = $price * ($count);
        $cart->update([
            'count' => $count,
            'total_price' => $totalPrice,
        ]);

        return response()->json(['total_price' => $totalPrice, 'count' => $count], 200);
    }
}

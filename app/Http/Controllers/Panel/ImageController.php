<?php

namespace App\Http\Controllers\Panel;

use App\ImageGallery;
use App\ImageGalleyProduct;
use App\Model\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    public function showImage($product_id)
    {
        $product=Product::find($product_id);
        $image=$product->gallery()->get();
        return response()->json($image, 200);
    }

    public function store(Request $request,$product_id)
    {
        $this->validate($request, [
            'file_name.*' => 'image|mimes:jpg,jpeg,png,gif,bmp'
        ]);
        $images = $this->uploadFiles($request);
        foreach ($images as $imageFile) {
            list($fileName) = $imageFile;
            $image = new ImageGallery();
            $image->file_name = $fileName;
            $image->save();
            $image->product()->sync($product_id);

        }

        return response()->json(['message' => 'success']);
    }

    protected function uploadFiles(Request $request)
    {
        $uploadedImages = [];
        if ($request->hasFile('file_name')) {
            $images = $request->file('file_name');
            foreach ($images as $image) {
                $uploadedImages[] = $this->uploadFile($image);
            }
        }
        return $uploadedImages;
    }

    protected function uploadFile($image)
    {
        $originalFileName = $image->getClientOriginalName();
        $extensions = $image->getClientOriginalExtension();
        $fileOnlyName = pathinfo($originalFileName, PATHINFO_FILENAME);
        $fileName = str_slug($fileOnlyName) . "-" . time() . "-" . $extensions;
        $uploadedFileName = $image->storeAs('uploads/Product', $fileName);
        return [$uploadedFileName, $fileOnlyName];
    }

    public function delete($id)
    {
        $p=ImageGalleyProduct::where('image_id',$id);
        $p->delete();
        $image = ImageGallery::find($id);
        if (file_exists($image->file_name)) {
            unlink($image->file_name);
        }
        $image->delete();

    }

}

<?php

namespace App\Http\Controllers\Panel;

use App\Model\Comment;
use App\Model\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function commentStore(Request $request)
    {
        $this->validate($request,[
           'massage' =>'required|min:2|max:191'
        ]);
        $comment = $request->all();
        $comment['user_id'] = auth()->user()->id;
        Comment::create($comment);
        return response()->json(['massage' => 'success']);
    }

    public function singleComment($id)
    {
        $massage = Comment::find($id);
        $massage['user_id'] = User::where('id', '=', $massage->user_id)->get();
        $massage['product_id'] = Product::with('category')->where('id', '=', $massage->product_id)->get();
        return response()->json($massage);
    }

    public function commentUpdate(Request $request, $id)
    {
        $massage = Comment::find($id);
        $massage->update([
            'status' => $request->input('status')
        ]);
        return response()->json(['massage' => 'success']);
    }

    public function commentList()
    {
        $massage = Comment::with(['user', 'product'=>function($cat){
            $cat->with("category");
        }])->get();
        return response()->json($massage);
    }

    public function showProductComment($id)
    {
        $massage = Comment::with('user','product')->where('product_id', '=', $id)
            ->where('status', '=', '1')->get();
        return response()->json($massage);
    }

    public function delete($id)
    {
        $massage =Comment::find($id);
        $massage->delete();
        return response()->json(['massage' => 'success']);
    }
}

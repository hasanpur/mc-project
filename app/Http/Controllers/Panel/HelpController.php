<?php

namespace App\Http\Controllers\Panel;

use App\Model\Help;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HelpController extends Controller
{
    public function store(Request $request)
    {
        Help::create($request->all());
        return response()->json(['message'=>'Success'],200);
    }

    public function list()
    {
        return response()->json(Help::all(),200);
    }

    public function single(Help $help)
    {
        return response()->json($help::first(),200);
    }
    public function update(Request $request,Help $help)
    {
        $help->update($request->all());
        return response()->json(['message'=>'Success'],200);
    }
    public function delete(Help $help)
    {
        $help->delete();
        return response()->json(['message'=>'Success'],200);

    }
}

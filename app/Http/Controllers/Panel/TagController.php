<?php

namespace App\Http\Controllers\Panel;

use App\Model\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{

    public function store(Request $request)
    {
        $this->validate($request,[
            'tag_title'=>'required|min:2|max:191'
        ]);
        Tag::create([
            'tag_title'=>$request->input('tag_title')
        ]);
        return response()->json(['massage'=>'success']);

    }
    public function single($id)
    {
        $tag=Tag::find($id);
        return response()->json($tag,200);

    }
    public function list()
    {
        $tag=Tag::all();
        return response()->json($tag,200);

    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'tag_title'=>'required|min:2|max:191'
        ]);
        $tag=Tag::find($id);
        $tag->update($request->all());
        return response()->json(['massage'=>'success']);
    }

    public function destroy($id)
    {
        $tag=Tag::find($id);
        $tag->delete();
        return response()->json(['massage'=>'success']);
    }
}

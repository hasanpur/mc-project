<?php

namespace App\Http\Controllers\Panel;

use App\Model\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function list()
    {
        return response()->json(Setting::all());

    }
    public function AboutUs()
    {
        $about_us=Setting::where('id','=',1)->get();
        return response()->json($about_us);
    }

    public function single($id)
    {
        $setting=Setting::find($id);
        return response()->json($setting,200);
    }

    public function update(Request $request,$id)
    {
        $setting=Setting::find($id);
        $setting->update($request);
        return response()->json(['massage'=>'success'],200);
    }

}

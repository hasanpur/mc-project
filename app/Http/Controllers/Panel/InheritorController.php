<?php

namespace App\Http\Controllers\Panel;

use App\Model\Inheritor;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InheritorController extends Controller
{

    public function store(Request $request)
    {
        $id = auth()->id();
        $user = User::find($id);
        $user->inheritor()->create([
            'name' => $request->input('name'),
            'last_name' => $request->input('last_name'),
            'password' => $request->input('password'),
            'national_code' => $request->input('national_code'),
            'mobile' => $request->input('mobile')
        ]);

    }

    public function single($id)
    {
        $inheritor = Inheritor::find($id);
        return response()->json($inheritor, 200);
    }

    public function update(Request $request, $id)
    {
        $id = auth()->id();
        $user = User::find($id);
        $user->inheritor()->update([
            'name' => $request->input('name'),
            'last_name' => $request->input('last_name'),
            'password' => $request->input('password'),
            'national_code' => $request->input('national_code'),
            'mobile' => $request->input('mobile')
        ]);
        return response()->json(['massage' => 'success'], 200);
    }

    public function delete($id)
    {
        $inheritor = Inheritor::find($id);
        $inheritor->delete();
        return response()->json(['massage' => 'success'], 200);
    }
}

<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Model\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function list()
    {
        $category=Category::all();
        return response()->json($category,200);
    }
    public function store(Request $request)
    {
        $this->validate($request,[
           'category_title' =>'required|min:2|max:191'
        ]);
        Category::create($request->all());
        return response()->json(['massage'=>'success']);
    }

    public function single($id)
    {
        $category=Category::find($id);
        return response()->json($category);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'category_title' =>'required|min:2|max:191'
        ]);

        $category=Category::find($id);
        $category->update([
            'category_title '=>$request->input('category_title'),
            'parent_id '=>$request->input('parent_id') ,
        ]);
        return response()->json(['massage'=>'success']);
    }

    public function destroy($id)
    {
        $category=Category::find($id);
        $category->delete();
        return response()->json(['massage'=>'success']);
    }

}

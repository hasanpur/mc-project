<?php

namespace App\Http\Controllers\Site;

use App\Model\ContactUs;
use App\Model\RelatedTo;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ContactUsController extends Controller
{
    public function list()
    {
        $contact=ContactUs::with('user','related_to')->get();
        return response()->json($contact, 200);
    }

    public function store(Request $request)
    {
        ContactUs::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'related_id' => $request->input('related_id'),
            'response' => $request->input('response'),
            'status' => $request->input('status')
        ]);
        return response()->json(['message' => 'Success'], 200);
    }


    public function single($id)
    {
        $contact=ContactUs::find($id);
        $contact['user_id'] = User::where('id', $contact->user_id)->first();
        $contact['related_id'] = RelatedTo::where('related_id', $contact->related_id)->first();
        return response()->json($contact, 200);
    }

    public function update(Request $request,$id)
    {
        $contact=ContactUs::find($id);

        if (is_null($request->input('response'))){
            $status = 'پاسخ داده نشده';
        }else{
            $status = 'پاسخ داده شده';
        }
        $contact->update([
            'response' => $request->input('response'),
            'status' => $status
        ]);
        return response()->json(['message' => 'Success'], 200);
    }

    public function delete(ContactUs $contactUs)
    {
        $contactUs->delete();
        return response()->json(['message' => 'Success'], 200);
    }
}

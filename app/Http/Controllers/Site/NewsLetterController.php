<?php

namespace App\Http\Controllers\Site;

use App\Model\NewsLetter;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NewsLetterController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request,[
            'email'=>'required|email'
        ]);
        NewsLetter::create($request->all());
        return redirect(route('site'));
    }

    public function join()
    {
        $id = Auth::id();
        $user = User::find($id);
        NewsLetter::create([
            'user_id'=> auth()->user()->id,
            'email' => auth()->user()->email
        ]);
        $user->update([
            'news_letter_status' => 1
        ]);
        return response()->json(['massage' => 'success']);
    }

    public function left()
    {
        $id = Auth::id();
        $user = User::find($id);
        $news=NewsLetter::where('email', auth()->user()->email)->first();
        $news->delete();
        $user->update([
            'news_letter_status' => 0
        ]);
        return response()->json(['massage' => 'success']);
    }

    public function list()
    {
        $newsLetter = NewsLetter::with('user')->get();
        return response()->json($newsLetter,200);
    }

    public function newsLetterUserList()
    {
        $newsLetter=NewsLetter::with('user')->where('user_id','!=',null)->get();
        return response()->json($newsLetter,200);
    }

    public function userNewsLetter()
    {
        $newsLetter = NewsLetter::where('email', '=',auth()->user()->email)->first();
        if (is_null($newsLetter)) {
            $answer = '0';
        } else {
            $answer = '1';
        }
        $data = ['newsLetter_status' => $answer];
        return response()->json($data,200);
    }
}

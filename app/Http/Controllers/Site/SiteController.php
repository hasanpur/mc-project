<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        return view('index');
        //        auth()->loginUsingId('1');
    }
    public function Home(){
        return view('home');
    }
    public function IncomePlan(){
        return view('IncomePlan.IncomePlan');
    }
    public function ShoppingHelp(){
        return view('ShoppingHelp.ShoppingHelp');
    }
    public function Privacy(){
        return view('Privacy.Privacy');
    }
    public function CallmePage()
    {
        return view('callmepage');
    }

    public function Store()
    {
        return view('store.store');
    }

    public function StoreWare()
    {
        return view('store.storeware');
    }

    public function SingleProduct()
    {
        return view('store.SingleProduct');
    }
    public function ContactUs()
    {
        return view('ContactUs.ContactUs');
    }
    public function AboutUs()
    {
        return view('AboutUs.AboutUs');
    }
    public function Blog()
    {
        return view('Blog.Blog');
    }
    public function CompareProduct()
    {
        return view('store.CompareProduct');
    }
    public function SingleBlog()
    {
        return view('Blog.SingleBlog');
    }
    public function ShoppingBasket()
    {
        return view('store.ShoppingBasket');
    }
    public function Login()
    {
        return view('auth.login');
    }

    public function Register()
    {
        return view('auth.register');
    }
    public function Reset()
    {
        return view('auth.passwords.reset');
    }

}
